package eu.k5.liquicheck;

import com.google.common.base.Objects;
import eu.k5.liquicheck.Environment;
import eu.k5.liquicheck.Form;
import eu.k5.liquicheck.Observers;
import eu.k5.liquicheck.events.ChangeEvent;
import eu.k5.liquicheck.events.ChangeSetEvent;
import eu.k5.liquicheck.events.ColumnConfigEvent;
import eu.k5.liquicheck.events.ColumnTypeEvent;
import eu.k5.liquicheck.events.CreateTableEvent;
import eu.k5.liquicheck.events.Event;
import eu.k5.liquicheck.events.IdentifierEvent;
import eu.k5.liquicheck.events.RenameEvent;
import eu.k5.liquicheck.model.Identifier;
import eu.k5.liquicheck.model.IdentifierType;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import liquibase.change.AbstractChange;
import liquibase.change.AddColumnConfig;
import liquibase.change.Change;
import liquibase.change.ColumnConfig;
import liquibase.change.core.AddColumnChange;
import liquibase.change.core.AddForeignKeyConstraintChange;
import liquibase.change.core.AddPrimaryKeyChange;
import liquibase.change.core.AddUniqueConstraintChange;
import liquibase.change.core.CreateTableChange;
import liquibase.change.core.CreateViewChange;
import liquibase.change.core.RenameColumnChange;
import liquibase.change.core.RenameTableChange;
import liquibase.change.core.RenameViewChange;
import liquibase.changelog.ChangeSet;
import liquibase.serializer.LiquibaseSerializable;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Conversions;

@SuppressWarnings("all")
public class ChangeSetDispatch implements Environment {
  private final Observers observers;
  
  private final Pattern dimensionalType;
  
  private final Deque<Change> changes;
  
  private final Deque<ChangeSet> changeSet;
  
  public ChangeSetDispatch(final Observers events) {
    this.observers = events;
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("(\\w+)\\((\\d+)(,\\d+)?\\)");
    Pattern _compile = Pattern.compile(_builder.toString());
    this.dimensionalType = _compile;
    ArrayDeque<Change> _arrayDeque = new ArrayDeque<Change>();
    this.changes = _arrayDeque;
    ArrayDeque<ChangeSet> _arrayDeque_1 = new ArrayDeque<ChangeSet>();
    this.changeSet = _arrayDeque_1;
  }
  
  public void start() {
    Iterable<Event> _forType = this.observers.<Event>forType(Event.class);
    final Consumer<Event> _function = new Consumer<Event>() {
      public void accept(final Event it) {
        it.start(ChangeSetDispatch.this);
      }
    };
    _forType.forEach(_function);
  }
  
  public void end() {
    Iterable<Event> _forType = this.observers.<Event>forType(Event.class);
    final Consumer<Event> _function = new Consumer<Event>() {
      public void accept(final Event it) {
        it.end();
      }
    };
    _forType.forEach(_function);
  }
  
  protected void _disp(final ChangeSet change) {
    this.changeSet.push(change);
    Iterable<ChangeSetEvent> _forType = this.observers.<ChangeSetEvent>forType(ChangeSetEvent.class);
    final Consumer<ChangeSetEvent> _function = new Consumer<ChangeSetEvent>() {
      public void accept(final ChangeSetEvent it) {
        it.startChangeSet(change);
      }
    };
    _forType.forEach(_function);
    List<Change> _changes = change.getChanges();
    final Consumer<Change> _function_1 = new Consumer<Change>() {
      public void accept(final Change it) {
        ChangeSetDispatch.this.dispatchChange(it);
      }
    };
    _changes.forEach(_function_1);
    Iterable<ChangeSetEvent> _forType_1 = this.observers.<ChangeSetEvent>forType(ChangeSetEvent.class);
    final Consumer<ChangeSetEvent> _function_2 = new Consumer<ChangeSetEvent>() {
      public void accept(final ChangeSetEvent it) {
        it.endChangeSet();
      }
    };
    _forType_1.forEach(_function_2);
    this.changeSet.pop();
  }
  
  public void dispatchChange(final Change change) {
    this.changes.push(change);
    Iterable<ChangeEvent> _forType = this.observers.<ChangeEvent>forType(ChangeEvent.class);
    final Consumer<ChangeEvent> _function = new Consumer<ChangeEvent>() {
      public void accept(final ChangeEvent it) {
        it.startChange(change);
      }
    };
    _forType.forEach(_function);
    this.disp(change);
    Iterable<ChangeEvent> _forType_1 = this.observers.<ChangeEvent>forType(ChangeEvent.class);
    final Consumer<ChangeEvent> _function_1 = new Consumer<ChangeEvent>() {
      public void accept(final ChangeEvent it) {
        it.endChange();
      }
    };
    _forType_1.forEach(_function_1);
    this.changes.pop();
  }
  
  protected void _disp(final AbstractChange change) {
    Class<? extends AbstractChange> _class = change.getClass();
    String _plus = ("unkown changeset " + _class);
    System.out.println(_plus);
  }
  
  protected void _disp(final CreateTableChange createTable) {
    Iterable<CreateTableEvent> _forType = this.observers.<CreateTableEvent>forType(CreateTableEvent.class);
    final Consumer<CreateTableEvent> _function = new Consumer<CreateTableEvent>() {
      public void accept(final CreateTableEvent it) {
        it.fireStart(createTable);
      }
    };
    _forType.forEach(_function);
    String _tableName = createTable.getTableName();
    this.dispIdentifier(IdentifierType.TABLE, _tableName);
    List<ColumnConfig> _columns = createTable.getColumns();
    final Consumer<ColumnConfig> _function_1 = new Consumer<ColumnConfig>() {
      public void accept(final ColumnConfig it) {
        ChangeSetDispatch.this.disp(it);
      }
    };
    _columns.forEach(_function_1);
    Iterable<CreateTableEvent> _forType_1 = this.observers.<CreateTableEvent>forType(CreateTableEvent.class);
    final Consumer<CreateTableEvent> _function_2 = new Consumer<CreateTableEvent>() {
      public void accept(final CreateTableEvent it) {
        it.fireEnd(createTable);
      }
    };
    _forType_1.forEach(_function_2);
  }
  
  protected void _disp(final RenameTableChange renameTable) {
    String _oldTableName = renameTable.getOldTableName();
    String _newTableName = renameTable.getNewTableName();
    this.renameIdentifier(IdentifierType.TABLE, _oldTableName, _newTableName);
  }
  
  protected void _disp(final RenameColumnChange renameColumn) {
    String _oldColumnName = renameColumn.getOldColumnName();
    String _newColumnName = renameColumn.getNewColumnName();
    this.renameIdentifier(IdentifierType.COLUMN, _oldColumnName, _newColumnName);
  }
  
  protected void _disp(final CreateViewChange createView) {
    String _viewName = createView.getViewName();
    this.dispIdentifier(IdentifierType.VIEW, _viewName);
  }
  
  protected void _disp(final RenameViewChange renameView) {
    String _oldViewName = renameView.getOldViewName();
    String _newViewName = renameView.getNewViewName();
    this.renameIdentifier(IdentifierType.VIEW, _oldViewName, _newViewName);
  }
  
  protected void _disp(final AddForeignKeyConstraintChange foreignKeyConstraintChange) {
    String _constraintName = foreignKeyConstraintChange.getConstraintName();
    this.dispIdentifier(IdentifierType.FOREIGN_KEY_CONSTRAINT, _constraintName);
  }
  
  protected void _disp(final AddPrimaryKeyChange primaryKeyChange) {
    String _constraintName = primaryKeyChange.getConstraintName();
    this.dispIdentifier(IdentifierType.PRIMERY_KEY_CONSTRAINT, _constraintName);
  }
  
  protected void _disp(final AddUniqueConstraintChange uniqueConstraintChange) {
    String _constraintName = uniqueConstraintChange.getConstraintName();
    this.dispIdentifier(IdentifierType.UNIQUE_CONSTRAINT, _constraintName);
  }
  
  protected void _disp(final AddColumnChange addColumn) {
    List<AddColumnConfig> _columns = addColumn.getColumns();
    final Consumer<AddColumnConfig> _function = new Consumer<AddColumnConfig>() {
      public void accept(final AddColumnConfig it) {
        ChangeSetDispatch.this.disp(it);
      }
    };
    _columns.forEach(_function);
  }
  
  protected void _disp(final ColumnConfig columnConfig) {
    Iterable<ColumnConfigEvent> _forType = this.observers.<ColumnConfigEvent>forType(ColumnConfigEvent.class);
    final Consumer<ColumnConfigEvent> _function = new Consumer<ColumnConfigEvent>() {
      public void accept(final ColumnConfigEvent it) {
        it.fire(columnConfig);
      }
    };
    _forType.forEach(_function);
    String _name = columnConfig.getName();
    this.dispIdentifier(IdentifierType.COLUMN, _name);
    String _type = columnConfig.getType();
    boolean _notEquals = (!Objects.equal(_type, null));
    if (_notEquals) {
      String _type_1 = columnConfig.getType();
      final Matcher matcherOne = this.dimensionalType.matcher(_type_1);
      boolean _matches = matcherOne.matches();
      if (_matches) {
        final String typeName = matcherOne.group(1);
        String _group = matcherOne.group(2);
        final int multiplicity = Integer.parseInt(_group);
        final List<Integer> mults = new ArrayList<Integer>();
        mults.add(Integer.valueOf(multiplicity));
        String _group_1 = matcherOne.group(3);
        boolean _notEquals_1 = (!Objects.equal(_group_1, null));
        if (_notEquals_1) {
          String _group_2 = matcherOne.group(3);
          String _substring = _group_2.substring(1);
          String[] _split = _substring.split(",");
          final Consumer<String> _function_1 = new Consumer<String>() {
            public void accept(final String it) {
              int _parseInt = Integer.parseInt(it);
              mults.add(Integer.valueOf(_parseInt));
            }
          };
          ((List<String>)Conversions.doWrapArray(_split)).forEach(_function_1);
        }
        Iterable<ColumnTypeEvent> _forType_1 = this.observers.<ColumnTypeEvent>forType(ColumnTypeEvent.class);
        final Consumer<ColumnTypeEvent> _function_2 = new Consumer<ColumnTypeEvent>() {
          public void accept(final ColumnTypeEvent it) {
            it.fire(typeName, mults);
          }
        };
        _forType_1.forEach(_function_2);
        return;
      } else {
        Iterable<ColumnTypeEvent> _forType_2 = this.observers.<ColumnTypeEvent>forType(ColumnTypeEvent.class);
        final Consumer<ColumnTypeEvent> _function_3 = new Consumer<ColumnTypeEvent>() {
          public void accept(final ColumnTypeEvent it) {
            String _type = columnConfig.getType();
            it.fire(_type);
          }
        };
        _forType_2.forEach(_function_3);
      }
    }
  }
  
  public void dispIdentifier(final IdentifierType type, final String identifier) {
    Iterable<IdentifierEvent> _forType = this.observers.<IdentifierEvent>forType(IdentifierEvent.class);
    final Consumer<IdentifierEvent> _function = new Consumer<IdentifierEvent>() {
      public void accept(final IdentifierEvent it) {
        Identifier _identifier = new Identifier(type, identifier);
        it.fire(_identifier);
      }
    };
    _forType.forEach(_function);
  }
  
  public void renameIdentifier(final IdentifierType type, final String oldIdentifier, final String newIdentifier) {
    Iterable<RenameEvent> _forType = this.observers.<RenameEvent>forType(RenameEvent.class);
    final Consumer<RenameEvent> _function = new Consumer<RenameEvent>() {
      public void accept(final RenameEvent it) {
        it.fire(type, oldIdentifier, newIdentifier);
      }
    };
    _forType.forEach(_function);
    this.dispIdentifier(type, newIdentifier);
  }
  
  public Change getChange() {
    boolean _isEmpty = this.changes.isEmpty();
    if (_isEmpty) {
      return null;
    }
    return this.changes.peek();
  }
  
  public ChangeSet getChangeSet() {
    boolean _isEmpty = this.changeSet.isEmpty();
    if (_isEmpty) {
      return null;
    }
    return this.changeSet.peek();
  }
  
  @Override
  public String toString() {
    return Form.at("{}>{}", this.changeSet, this.changes);
  }
  
  public void disp(final LiquibaseSerializable addColumn) {
    if (addColumn instanceof AddColumnChange) {
      _disp((AddColumnChange)addColumn);
      return;
    } else if (addColumn instanceof AddForeignKeyConstraintChange) {
      _disp((AddForeignKeyConstraintChange)addColumn);
      return;
    } else if (addColumn instanceof AddPrimaryKeyChange) {
      _disp((AddPrimaryKeyChange)addColumn);
      return;
    } else if (addColumn instanceof AddUniqueConstraintChange) {
      _disp((AddUniqueConstraintChange)addColumn);
      return;
    } else if (addColumn instanceof CreateTableChange) {
      _disp((CreateTableChange)addColumn);
      return;
    } else if (addColumn instanceof CreateViewChange) {
      _disp((CreateViewChange)addColumn);
      return;
    } else if (addColumn instanceof RenameColumnChange) {
      _disp((RenameColumnChange)addColumn);
      return;
    } else if (addColumn instanceof RenameTableChange) {
      _disp((RenameTableChange)addColumn);
      return;
    } else if (addColumn instanceof RenameViewChange) {
      _disp((RenameViewChange)addColumn);
      return;
    } else if (addColumn instanceof AbstractChange) {
      _disp((AbstractChange)addColumn);
      return;
    } else if (addColumn instanceof ColumnConfig) {
      _disp((ColumnConfig)addColumn);
      return;
    } else if (addColumn instanceof ChangeSet) {
      _disp((ChangeSet)addColumn);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(addColumn).toString());
    }
  }
}
