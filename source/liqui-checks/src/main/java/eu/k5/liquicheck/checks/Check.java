package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.events.Event;

public interface Check {

    String getId();

    Event create(LiquiViolations violations);
}
