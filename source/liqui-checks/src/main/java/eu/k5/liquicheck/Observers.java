package eu.k5.liquicheck;

import com.google.common.collect.ImmutableMultimap;
import eu.k5.liquicheck.events.Event;
import java.util.Collection;
import java.util.Iterator;

public class Observers {

    private final ImmutableMultimap<Class<?>, Event> observers;

    public Observers(ImmutableMultimap<Class<?>, Event> observers) {
        this.observers = observers;
    }

    private static class ObserverIterable<E> implements Iterable<E> {

        private final Collection<Event> entries;
        private final Class<E> type;

        public ObserverIterable(Collection<Event> entries, Class<E> type) {
            this.entries = entries;
            this.type = type;
        }

        @Override
        public Iterator<E> iterator() {
            return new ObserverIterator<E>(entries.iterator(), type);
        }

    }

    private static class ObserverIterator<E> implements Iterator<E> {

        private final Iterator<Event> entries;
        private final Class<E> type;

        public ObserverIterator(Iterator<Event> entries, Class<E> type) {
            this.entries = entries;
            this.type = type;
        }

        @Override
        public boolean hasNext() {
            return entries.hasNext();
        }

        @Override
        public E next() {
            return type.cast(entries.next());
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not implemented");
        }

    }

    public <E extends Event> Iterable<E> forType(Class<E> type) {
        return new ObserverIterable<>(observers.get(type), type);
    }
}
