package eu.k5.liquicheck.events;

import eu.k5.liquicheck.model.IdentifierType;

public interface RenameEvent extends Event {

    void fire(IdentifierType type, String oldIdentifier, String newIdentifier);

}
