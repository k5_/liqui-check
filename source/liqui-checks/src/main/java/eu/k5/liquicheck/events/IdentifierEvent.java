package eu.k5.liquicheck.events;

import eu.k5.liquicheck.model.Identifier;

public interface IdentifierEvent extends Event {

    void fire(Identifier identifier);

}
