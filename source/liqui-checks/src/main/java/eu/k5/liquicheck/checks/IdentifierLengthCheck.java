package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.events.Event;
import eu.k5.liquicheck.events.IdentifierEvent;
import eu.k5.liquicheck.model.Identifier;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class IdentifierLengthCheck extends AbstractCheck {

    @XmlAttribute
    private int maxLength;

    @XmlElement
    private Limit table;
    private Limit column;

    static class IdentifierLength extends AbstractCheckEvent implements IdentifierEvent {

        private final int maxLength;

        public IdentifierLength(int maxLength, LiquiViolations violations) {
            super(violations);
            this.maxLength = maxLength;
        }

        @Override
        public void fire(Identifier identifier) {
            if (identifier.getName().length() > maxLength) {
                error("Identifier '{}' longer than {} characters", identifier.getName(), maxLength);
            }
        }
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    @Override
    public Event create(LiquiViolations violations) {
        return new IdentifierLength(maxLength, violations);
    }

}
