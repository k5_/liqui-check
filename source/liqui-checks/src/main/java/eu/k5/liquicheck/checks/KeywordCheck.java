package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.events.IdentifierEvent;
import eu.k5.liquicheck.model.Identifier;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class KeywordCheck extends AbstractCheck {

    @XmlElement(name = "word")
    private List<String> keywords;

    public List<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<String> keywords) {
        this.keywords = keywords;
    }

    static class Id {

    }

    static class Keyword extends AbstractCheckEvent implements IdentifierEvent {

        private final Set<String> keywords;

        public Keyword(Set<String> keywords, LiquiViolations violations) {
            super(violations);
            this.keywords = keywords;

        }

        @Override
        public void fire(Identifier identifier) {
            String name = identifier.getName();
            if (keywords.contains(name.toUpperCase())) {
                error("Identifier is a keyword {}", identifier.getName());
            }
        }

    }

    @Override
    public Keyword create(LiquiViolations violations) {
        Set<String> words = new HashSet<>(keywords);

        return new Keyword(words, violations);
    }

}
