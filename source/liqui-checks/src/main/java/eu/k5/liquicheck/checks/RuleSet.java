package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RuleSet {

    @XmlElement(name = "import", type = RuleReference.class)
    private List<RuleReference> imports = new ArrayList<>();

    @XmlElement(name = "use")
    private List<RuleUse> uses = new ArrayList<>();

    @XmlElements({
        @XmlElement(name = "keywords", type = KeywordCheck.class),
        @XmlElement(name = "nameConventions", type = NameConventionsCheck.class),
        @XmlElement(name = "allowedTypes", type = AllowedTypeCheck.class),
        @XmlElement(name = "identifierLength", type = IdentifierLengthCheck.class)})
    private List<Check> checks;

    @XmlAttribute
    private String id;

    public RuleSet() {
        // for JAXB
    }

    public RuleSet(String id, List<Check> checks, List<RuleReference> imports, List<RuleUse> uses) {
        this.imports = imports;
        this.uses = uses;
        this.checks = checks;
        this.id = id;
    }

    public List<Check> getChecks() {
        return checks;
    }

    public String getId() {
        return id;
    }

    public List<RuleReference> getImports() {
        return imports;
    }

    public List<RuleUse> getUses() {
        return uses;
    }

    public Check getCheck(String checkId) {
        for (Check check : checks) {
            if (check.getId().equals(checkId)) {
                return check;
            }
        }
        return null;
    }

    public static RuleSet loadFromClasspath(String resource) throws IOException, JAXBException {
        try (InputStream is = RuleSet.class.getResourceAsStream(resource);
                Reader reader = new InputStreamReader(is, "utf8")) {
            Unmarshaller unmarshaller = JAXBContext.newInstance(RuleSet.class).createUnmarshaller();
            return (RuleSet) unmarshaller.unmarshal(reader);
        }
    }

    public static class Builder {

        private String id = "Generated-id-" + UUID.randomUUID();
        private final Map<String, Check> checks = new HashMap<>();
        private final LiquiViolations violations;
        private final Map<String, RuleSet> imported = new HashMap<>();

        public Builder() {
            this(new LiquiViolations());
        }

        public Builder(LiquiViolations violations) {
            this.violations = violations;
            initStandardRuleSets();

        }

        private void initStandardRuleSets() {
            for (StandardRuleSets ruleSet : StandardRuleSets.values()) {
                imported.put(ruleSet.getRuleSet().getId(), ruleSet.getRuleSet());
            }
        }

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder addRuleSet(RuleSet ruleSet) {
            for (RuleReference reference : ruleSet.getImports()) {
                load(reference);
            }
            for (RuleUse use : ruleSet.getUses()) {
                register(use);
            }
            for (Check check : ruleSet.getChecks()) {
                addCheck(check);
            }

            imported.put(ruleSet.getId(), ruleSet);
            return this;
        }

        private void register(RuleUse use) {
            RuleSet ruleSet = imported.get(use.getRuleSetId());
            if (ruleSet != null) {
                Check check = ruleSet.getCheck(use.getRule());
                if (check == null) {
                    violations.info(null, "Unable to resolve Rule {} in RuleSet {}", use.getRule(), use.getRuleSetId());
                } else {
                    addCheck(check);
                }
            } else {
                violations.info(null, "RuleSet {} not imported", use.getRuleSetId());
            }
        }

        private void load(RuleReference reference) {
            try {
                imported.put(reference.getId(), RuleSet.loadFromClasspath(reference.getResource()));
            } catch (IOException | JAXBException e) {
                violations.info(null, "Unable to load RuleSet {}", reference.getResource());
            }
        }

        public Builder addCheck(Check check) {
            checks.put(check.getId(), check);
            return this;
        }

        public LiquiViolations getViolations() {
            return violations;
        }

        public RuleSet build() {
            return new RuleSet(id, new ArrayList<>(checks.values()), new ArrayList<RuleReference>(), new ArrayList<RuleUse>());
        }
    }
}
