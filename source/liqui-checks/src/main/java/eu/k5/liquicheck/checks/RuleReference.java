package eu.k5.liquicheck.checks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class RuleReference {

    @XmlAttribute
    private String id;

    @XmlAttribute
    private String resource;

    public String getId() {
        return id;
    }

    public String getResource() {
        return resource;
    }

}
