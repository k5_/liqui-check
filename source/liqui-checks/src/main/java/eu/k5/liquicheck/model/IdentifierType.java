package eu.k5.liquicheck.model;

public enum IdentifierType {
    INDEX, SEQUENCE, VIEW, TABLE, COLUMN, UNIQUE_CONSTRAINT, PRIMERY_KEY_CONSTRAINT, FOREIGN_KEY_CONSTRAINT;
}
