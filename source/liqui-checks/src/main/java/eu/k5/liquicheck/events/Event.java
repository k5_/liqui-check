package eu.k5.liquicheck.events;

import eu.k5.liquicheck.Environment;

public interface Event {

    void start(Environment environment);

    void end();

}
