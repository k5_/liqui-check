package eu.k5.liquicheck.events;

import liquibase.changelog.ChangeSet;

public interface ChangeSetEvent extends Event {

    void startChangeSet(ChangeSet changeSet);

    void endChangeSet();
}
