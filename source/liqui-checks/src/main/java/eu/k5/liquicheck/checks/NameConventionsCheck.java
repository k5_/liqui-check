package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.events.IdentifierEvent;
import eu.k5.liquicheck.events.RenameEvent;
import eu.k5.liquicheck.model.Identifier;
import eu.k5.liquicheck.model.IdentifierType;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class NameConventionsCheck extends AbstractCheck {

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class NamePattern {

        @XmlAttribute
        private String pattern;

        public NamePattern() {

        }

        public NamePattern(String pattern) {
            this.pattern = pattern;

        }

        public String getPattern() {
            return pattern;
        }

        public void setPattern(String pattern) {
            this.pattern = pattern;
        }

        public Pattern getCompiled() {
            return Pattern.compile(pattern);
        }
    }

    @XmlElement
    private NamePattern tableName = new NamePattern(".*");
    @XmlElement
    private NamePattern columnName = new NamePattern(".*");
    @XmlElement
    private NamePattern viewName = new NamePattern(".*");
    @XmlElement
    private NamePattern foreignKey = new NamePattern(".*");
    @XmlElement
    private NamePattern uniqueKey = new NamePattern(".*");

    public NamePattern getTableName() {
        return tableName;
    }

    public void setTableName(NamePattern tableName) {
        this.tableName = tableName;
    }

    public NamePattern getColumnName() {
        return columnName;
    }

    public void setColumnName(NamePattern columnName) {
        this.columnName = columnName;
    }

    public NamePattern getViewName() {
        return viewName;
    }

    public void setViewName(NamePattern viewName) {
        this.viewName = viewName;
    }

    public NamePattern getForeignKey() {
        return foreignKey;
    }

    public void setForeignKey(NamePattern foreignKey) {
        this.foreignKey = foreignKey;
    }

    public NamePattern getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(NamePattern uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    static class NameConvention extends AbstractCheckEvent implements IdentifierEvent, RenameEvent {

        private final Pattern tablePattern;
        private final Pattern columnPattern;
        private final Pattern viewPattern;
        private final Pattern foreignKeyPattern;
        private final Pattern uniqueKeyPattern;

        private final Map<Identifier, String> violations = new HashMap<>();

        public NameConvention(LiquiViolations violations, Pattern tablePattern, Pattern columnPattern,
                Pattern viewPattern, Pattern foreignKeyPattern, Pattern uniqueKeyPattern) {
            super(violations);
            this.tablePattern = tablePattern;
            this.columnPattern = columnPattern;
            this.viewPattern = viewPattern;
            this.foreignKeyPattern = foreignKeyPattern;
            this.uniqueKeyPattern = uniqueKeyPattern;
        }

        @Override
        public void fire(Identifier identifier) {
            switch (identifier.getType()) {
                case COLUMN:
                    if (!columnPattern.matcher(identifier.getName()).matches()) {
                        violations.put(identifier, "Identifier {} does not match column nameconventions");
                    }
                    break;
                case TABLE:
                    if (!tablePattern.matcher(identifier.getName()).matches()) {
                        violations.put(identifier, "Identifier {} does not match table nameconventions");
                    }
                    break;
                case VIEW:
                    if (!viewPattern.matcher(identifier.getName()).matches()) {
                        violations.put(identifier, "Identifier {} does not match view nameconventions");
                    }
                    break;
                case FOREIGN_KEY_CONSTRAINT:
                    if (!foreignKeyPattern.matcher(identifier.getName()).matches()) {
                        violations.put(identifier, "Identifier {} does not match foreign key nameconventions");
                    }

                    break;
                case UNIQUE_CONSTRAINT:
                    if (!uniqueKeyPattern.matcher(identifier.getName()).matches()) {
                        violations.put(identifier, "Identifier {} does not match unique keynameconventions");
                    }
                default:

                    break;

            }

        }

        @Override
        public void fire(IdentifierType type, String oldIdentifier, String newIdentifier) {
            violations.remove(new Identifier(type, oldIdentifier));
        }

        @Override
        public void end() {
            for (Entry<Identifier, String> v : violations.entrySet()) {
                error(v.getValue(), v.getKey().getName());
            }
        }
    }

    @Override
    public NameConvention create(LiquiViolations violations) {

        return new NameConvention(violations, tableName.getCompiled(), columnName.getCompiled(),
                viewName.getCompiled(), foreignKey.getCompiled(), uniqueKey.getCompiled());
    }

}
