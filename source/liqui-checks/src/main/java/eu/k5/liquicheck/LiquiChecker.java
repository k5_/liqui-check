package eu.k5.liquicheck;

import javax.xml.bind.JAXBException;
import liquibase.Liquibase;
import liquibase.changelog.ChangeSet;
import liquibase.changelog.DatabaseChangeLog;
import liquibase.database.Database;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;

public class LiquiChecker {

    private final Observers observer;
    private final LiquiViolations violations;

    public LiquiChecker(LiquiViolations violations, Observers observer) {
        this.violations = violations;
        this.observer = observer;

    }

    public LiquiViolations getViolations() {
        return violations;
    }

    public LiquiViolations check(String file) {
        try {
            ChangeSetDispatch dispatch = new ChangeSetDispatch(observer);

            dispatch.start();

            Liquibase base = new Liquibase(file, new ClassLoaderResourceAccessor(), (Database) null);
            DatabaseChangeLog databaseChangeLog = base.getDatabaseChangeLog();

            for (ChangeSet cs : databaseChangeLog.getChangeSets()) {
                dispatch.disp(cs);
            }

            dispatch.end();
        } catch (LiquibaseException ex) {
            violations.error(null, "Liquibase error: {}", ex);
        }
        return violations;
    }

    public static CheckerBuilder builder() throws JAXBException {
        return new CheckerBuilder();
    }
}
