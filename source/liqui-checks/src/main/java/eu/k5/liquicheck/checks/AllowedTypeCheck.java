package eu.k5.liquicheck.checks;

import com.google.common.base.Joiner;
import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.events.ColumnTypeEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class AllowedTypeCheck extends AbstractCheck {

    @XmlAttribute
    private boolean caseSensitive;

    @XmlElement(name = "type", type = Type.class)
    private List<Type> types;

    @XmlAccessorType(XmlAccessType.FIELD)
    static class Type {

        @XmlAttribute
        private String name;

        @XmlElement(name = "limit")
        private List<Limit> limits = new ArrayList<>();

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Limit> getLimits() {
            return limits;
        }

        public void setLimits(List<Limit> limits) {
            this.limits = limits;
        }

        public String asString() {
            return name + "(" + Joiner.on(',').join(limits) + ")";
        }

    }

    static class AllowedType extends AbstractCheckEvent implements ColumnTypeEvent {

        private final Map<String, Type> allowed;
        private final boolean caseSensitive;

        public AllowedType(LiquiViolations violations, boolean caseSensitive, Map<String, Type> allowed) {
            super(violations);
            this.caseSensitive = caseSensitive;
            this.allowed = allowed;

        }

        @Override
        public void fire(String name) {

            Type type = allowed.get(name(name));
            if (type == null) {
                error("{} is not an allowed type", name);
            } else if (!type.getLimits().isEmpty()) {
                error("{} is not allowed without multiplicity, expected ", name, type.asString());
            }
        }

        private String name(String name) {
            if (!caseSensitive) {
                return name.toUpperCase();
            } else {
                return name;
            }
        }

        @Override
        public void fire(String name, List<Integer> multiplicity) {
            Type type = allowed.get(name(name));
            if (type == null) {
                error("{} is not an allowed type", name);

            } else if (type.getLimits().size() != multiplicity.size()) {
                error("Type-limits: {} expected multiplicity-size:{}", type.getLimits().size(), multiplicity.size());

            } else {
                for (int l = 0; l < type.getLimits().size(); l++) {
                    Limit limit = type.getLimits().get(l);
                    if (!limit.inRange(multiplicity.get(l))) {
                        error("{} is not allowed with {} multiplicity at index, min {} max {}", name, multiplicity,
                                limit.getMin(), limit.getMax());
                    }
                }
            }
        }
    }

    public List<Type> getTypes() {
        return types;
    }

    public void setTypes(List<Type> types) {
        this.types = types;
    }

    @Override
    public AllowedType create(LiquiViolations violations) {
        Map<String, Type> allowed = new HashMap<>();
        for (Type type : types) {
            String name = type.getName();
            if (!caseSensitive) {
                name = name.toUpperCase();
            }
            allowed.put(type.getName(), type);
        }
        return new AllowedType(violations, caseSensitive, allowed);
    }
}
