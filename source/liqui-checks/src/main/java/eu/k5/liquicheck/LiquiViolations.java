package eu.k5.liquicheck;

import java.util.ArrayList;
import java.util.List;

public class LiquiViolations {

    public static class Violation {

        private final String changeSet;
        private final String change;

        private final Severity severity;

        private final String message;

        public Violation(Environment environment, Severity severity, String message) {
            if (environment != null && environment.getChangeSet() != null) {
                changeSet = environment.getChangeSet().toString();
            } else {
                changeSet = "unknown";
            }
            if (environment != null && environment.getChange() != null) {
                change = environment.getChange().getSerializedObjectName();
            } else {
                change = "unknown";
            }
            this.severity = severity;
            this.message = message;
        }

        public Severity getSeverity() {
            return severity;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return Form.at("[{}] {}>{} {}", severity, changeSet, change, message);
        }

    }

    public enum Severity {
        NONE(0), INFO(2), WARN(4), ERROR(16);
        private final int priority;

        private Severity(int priority) {
            this.priority = priority;
        }

        public int getPriority() {
            return priority;
        }
    }

    private final List<Violation> violations = new ArrayList<>();

    public void info(Environment env, String message, Object... arguments) {
        violations.add(new Violation(env, Severity.INFO, Form.at(message, arguments)));
    }

    public void warn(Environment env, String message, Object... arguments) {
        violations.add(new Violation(env, Severity.WARN, Form.at(message, arguments)));
    }

    public void error(Environment env, String message, Object... arguments) {
        violations.add(new Violation(env, Severity.ERROR, Form.at(message, arguments)));
    }

    public List<Violation> getViolations() {
        return violations;
    }

    public Severity getMaxSeverity() {
        Severity severity = Severity.NONE;
        for (Violation v : violations) {
            if (v.getSeverity().getPriority() > severity.getPriority()) {
                severity = v.getSeverity();
            }
        }
        return severity;
    }
}
