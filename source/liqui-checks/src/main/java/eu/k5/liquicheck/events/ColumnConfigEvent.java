package eu.k5.liquicheck.events;

import liquibase.change.ColumnConfig;

public interface ColumnConfigEvent extends Event {

    void fire(ColumnConfig config);

}
