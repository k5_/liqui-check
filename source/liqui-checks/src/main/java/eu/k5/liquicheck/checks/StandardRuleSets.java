package eu.k5.liquicheck.checks;

import java.io.IOException;
import javax.xml.bind.JAXBException;

public enum StandardRuleSets {
    MSSQL("/liquicheck/mssql.xml"), ORACLE("/liquicheck/oracle.xml");

    private RuleSet ruleSet;

    private StandardRuleSets(String resource) {
        try {
            this.ruleSet = RuleSet.loadFromClasspath(resource);
        } catch (IOException | JAXBException ex) {
            throw new RuntimeException(ex);
        }
    }

    public RuleSet getRuleSet() {
        return ruleSet;
    }

}
