package eu.k5.liquicheck;

import com.beust.jcommander.JCommander;
import eu.k5.liquicheck.LiquiViolations.Violation;
import java.io.IOException;
import javax.xml.bind.JAXBException;
import liquibase.exception.LiquibaseException;

public class Main {

    public static void main(String[] args) throws JAXBException, LiquibaseException, IOException {

        Param param = new Param();
        new JCommander(param, args);

        System.out.println(param.getChangelog());
        System.out.println(param.getRules());

        CheckerBuilder builder = new CheckerBuilder();
        builder.fromClasspath(param.getRules());

        LiquiChecker checker = builder.build();
        LiquiViolations violations = checker.check(param.getChangelog());

        for (Violation v : violations.getViolations()) {
            System.out.println(v.toString());
        }

    }
}
