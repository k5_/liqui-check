/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.k5.liquicheck.checks;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 *
 * @author Dominik Schlosser
 */
@XmlAccessorType(value = XmlAccessType.FIELD)
class Limit {

    @XmlAttribute
    private int max;
    @XmlAttribute
    private int min;

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public boolean inRange(int multiplicity) {
        return multiplicity >= min && multiplicity <= max;
    }

    @Override
    public String toString() {
        return min + ".." + max;
    }

}
