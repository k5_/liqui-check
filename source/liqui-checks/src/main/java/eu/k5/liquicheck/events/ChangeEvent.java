package eu.k5.liquicheck.events;

import liquibase.change.Change;

public interface ChangeEvent extends Event {

    void startChange(Change change);

    void endChange();
}
