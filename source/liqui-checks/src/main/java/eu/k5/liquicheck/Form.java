package eu.k5.liquicheck;

import org.slf4j.helpers.MessageFormatter;

class Form {

    public static String at(String message, Object... arguments) {
        return MessageFormatter.arrayFormat(message, arguments).getMessage();
    }
}
