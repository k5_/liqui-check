package eu.k5.liquicheck.events;

import java.util.List;

public interface ColumnTypeEvent extends Event {

    void fire(String type, List<Integer> multiplicity);

    void fire(String type);

}
