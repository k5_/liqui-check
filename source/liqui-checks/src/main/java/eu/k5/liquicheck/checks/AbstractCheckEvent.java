package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.Environment;
import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.events.Event;

public class AbstractCheckEvent implements Event {

    private Environment environment;
    private final LiquiViolations violations;

    public AbstractCheckEvent(LiquiViolations violations) {
        this.violations = violations;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public LiquiViolations getViolations() {
        return violations;
    }

    @Override
    public void start(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void end() {
    }

    public void error(String message, Object... arguments) {
        violations.error(environment, message, arguments);
    }
}
