package eu.k5.liquicheck;

import liquibase.change.Change;
import liquibase.changelog.ChangeSet;

public interface Environment {

    ChangeSet getChangeSet();

    Change getChange();
}
