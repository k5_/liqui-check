package eu.k5.liquicheck.events;

import liquibase.change.core.CreateTableChange;

public interface CreateTableEvent extends Event {

    void fireStart(CreateTableChange change);

    void fireEnd(CreateTableChange change);
}
