package eu.k5.liquicheck;

import com.google.common.collect.ImmutableMultimap;
import eu.k5.liquicheck.checks.Check;
import eu.k5.liquicheck.checks.RuleSet;
import eu.k5.liquicheck.checks.StandardRuleSets;
import eu.k5.liquicheck.events.Event;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.Set;
import javax.xml.bind.JAXBException;

public class CheckerBuilder {

    private final ImmutableMultimap.Builder<Class<?>, Event> observers = ImmutableMultimap.builder();

    private final LiquiViolations violations = new LiquiViolations();
    private final RuleSet.Builder ruleSetBuilder = new RuleSet.Builder(violations);

    public CheckerBuilder fromClasspath(String resource) {

        try {
            ruleSetBuilder.addRuleSet(RuleSet.loadFromClasspath(resource));
        } catch (IOException | JAXBException e) {
            violations.error(null, "Unable to unmarshall RuleSet", e);
        }

        return this;
    }

    public CheckerBuilder addRuleSet(StandardRuleSets ruleSet) {
        return addRuleSet(ruleSet.getRuleSet());
    }

    public CheckerBuilder addRuleSet(RuleSet ruleSet) {
        ruleSetBuilder.addRuleSet(ruleSet);
        return this;
    }

    public CheckerBuilder addEvents(Event... events) {
        for (Event event : events) {
            register(event);
        }
        return this;
    }

    private void register(Event event) {
        Deque<Class<?>> todo = new ArrayDeque<>();
        todo.addAll(Arrays.asList(event.getClass().getInterfaces()));

        Set<Class<?>> done = new HashSet<>();

        while (!todo.isEmpty()) {

            Class<?> iface = todo.pop();
            if (!done.contains(iface)) {
                done.add(iface);
                todo.addAll(Arrays.asList(iface.getInterfaces()));

                if (Event.class.isAssignableFrom(iface)) {
                    observers.put(iface, event);
                }
            }
        }
    }

    public LiquiChecker build() {
        RuleSet combinedRuleSet = ruleSetBuilder.build();
        for (Check check : combinedRuleSet.getChecks()) {
            register(check.create(violations));
        }

        return new LiquiChecker(violations, new Observers(observers.build()));
    }
}
