package eu.k5.liquicheck;

import com.beust.jcommander.Parameter;

public class Param {

    @Parameter(names = {"-r", "-rules"}, description = "Rules to use")
    private String rules;

    @Parameter(names = {"-c", "-changelog"}, description = "Changelogs")
    private String changelog;

    public String getChangelog() {
        return changelog;
    }

    public String getRules() {
        return rules;
    }

}
