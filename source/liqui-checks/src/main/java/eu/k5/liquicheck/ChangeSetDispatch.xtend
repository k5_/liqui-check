package eu.k5.liquicheck

import eu.k5.liquicheck.events.ChangeEvent
import eu.k5.liquicheck.events.ChangeSetEvent
import eu.k5.liquicheck.events.ColumnConfigEvent
import eu.k5.liquicheck.events.ColumnTypeEvent
import eu.k5.liquicheck.events.CreateTableEvent
import eu.k5.liquicheck.events.Event
import eu.k5.liquicheck.events.IdentifierEvent
import eu.k5.liquicheck.events.RenameEvent
import eu.k5.liquicheck.model.Identifier
import eu.k5.liquicheck.model.IdentifierType
import java.util.ArrayDeque
import java.util.ArrayList
import java.util.Deque
import java.util.List
import java.util.regex.Pattern
import liquibase.change.AbstractChange
import liquibase.change.Change
import liquibase.change.ColumnConfig
import liquibase.change.core.AddColumnChange
import liquibase.change.core.AddForeignKeyConstraintChange
import liquibase.change.core.AddPrimaryKeyChange
import liquibase.change.core.AddUniqueConstraintChange
import liquibase.change.core.CreateTableChange
import liquibase.change.core.CreateViewChange
import liquibase.change.core.RenameColumnChange
import liquibase.change.core.RenameTableChange
import liquibase.change.core.RenameViewChange
import liquibase.changelog.ChangeSet

class ChangeSetDispatch implements Environment {
	val Observers observers;
	val Pattern dimensionalType;
	val Deque<Change> changes;
	val Deque<ChangeSet> changeSet;

	new(Observers events) {
		this.observers = events;
		dimensionalType = Pattern.compile('''(\w+)\((\d+)(,\d+)?\)''');
		changes = new ArrayDeque<Change>();
		changeSet = new ArrayDeque<ChangeSet>();
	}

	def void start() {
		observers.forType(typeof(Event)).forEach[start(this)];
	}

	def void end() {
		observers.forType(typeof(Event)).forEach[end()];
	}

	def dispatch void disp(ChangeSet change) {
		changeSet.push(change);
		observers.forType(typeof(ChangeSetEvent)).forEach[startChangeSet(change)];

		change.changes.forEach[dispatchChange()];

		observers.forType(typeof(ChangeSetEvent)).forEach[endChangeSet()];
		changeSet.pop();
	}

	def void dispatchChange(Change change) {
		changes.push(change);
		observers.forType(typeof(ChangeEvent)).forEach[startChange(change)];

		disp(change);

		observers.forType(typeof(ChangeEvent)).forEach[endChange()];
		changes.pop();
	}

	def dispatch void disp(AbstractChange change) {
		System.out.println("unkown changeset " + change.class);
	}

	def dispatch void disp(CreateTableChange createTable) {
		observers.forType(typeof(CreateTableEvent)).forEach[fireStart(createTable)];

		dispIdentifier(IdentifierType.TABLE, createTable.tableName);
		createTable.columns.forEach[disp()];

		observers.forType(typeof(CreateTableEvent)).forEach[fireEnd(createTable)];
	}

	def dispatch void disp(RenameTableChange renameTable) {
		renameIdentifier(IdentifierType.TABLE, renameTable.oldTableName, renameTable.newTableName);
	}

	def dispatch void disp(RenameColumnChange renameColumn) {
		renameIdentifier(IdentifierType.COLUMN, renameColumn.oldColumnName, renameColumn.newColumnName);
	}

	def dispatch void disp(CreateViewChange createView) {
		dispIdentifier(IdentifierType.VIEW, createView.viewName);
	}

	def dispatch void disp(RenameViewChange renameView) {
		renameIdentifier(IdentifierType.VIEW, renameView.oldViewName, renameView.newViewName);
	}

	def dispatch void disp(AddForeignKeyConstraintChange foreignKeyConstraintChange) {
		dispIdentifier(IdentifierType.FOREIGN_KEY_CONSTRAINT, foreignKeyConstraintChange.constraintName);
	}

	def dispatch void disp(AddPrimaryKeyChange primaryKeyChange) {
		dispIdentifier(IdentifierType.PRIMERY_KEY_CONSTRAINT, primaryKeyChange.constraintName);
	}

	def dispatch void disp(AddUniqueConstraintChange uniqueConstraintChange) {
		dispIdentifier(IdentifierType.UNIQUE_CONSTRAINT, uniqueConstraintChange.constraintName);
	}

	def dispatch void disp(AddColumnChange addColumn) {
		addColumn.columns.forEach[disp()];
	}

	def dispatch void disp(ColumnConfig columnConfig) {
		observers.forType(typeof(ColumnConfigEvent)).forEach[fire(columnConfig)];
		dispIdentifier(IdentifierType.COLUMN, columnConfig.name);
		if (columnConfig.type != null) {
			val matcherOne = dimensionalType.matcher(columnConfig.type)
			if (matcherOne.matches) {
				val typeName = matcherOne.group(1)
				val multiplicity = Integer.parseInt(matcherOne.group(2));

				val List<Integer> mults = new ArrayList<Integer>();
				mults.add(multiplicity);

				if (matcherOne.group(3) != null) {
					matcherOne.group(3).substring(1).split(',').forEach[mults.add(Integer.parseInt(it))];
				}

				observers.forType(typeof(ColumnTypeEvent)).forEach[fire(typeName, mults)];
				return;
			} else {
				observers.forType(typeof(ColumnTypeEvent)).forEach[fire(columnConfig.type)];
			}
		}
	}

	def void dispIdentifier(IdentifierType type, String identifier) {
		observers.forType(typeof(IdentifierEvent)).forEach[fire(new Identifier(type, identifier))];
	}

	def void renameIdentifier(IdentifierType type, String oldIdentifier, String newIdentifier) {
		observers.forType(typeof(RenameEvent)).forEach[fire(type, oldIdentifier, newIdentifier)];
		dispIdentifier(type, newIdentifier);
	}

	override getChange() {
		if (changes.empty) {
			return null;

		}

		return changes.peek();
	}

	override getChangeSet() {
		if (changeSet.empty) {
			return null;
		}
		return changeSet.peek();
	}

	@Override
	override String toString() {
		return Form.at("{}>{}", changeSet, changes);
	}

}
