package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.checks.NameConventionsCheck.NameConvention;
import eu.k5.liquicheck.checks.NameConventionsCheck.NamePattern;
import eu.k5.liquicheck.model.Identifier;
import eu.k5.liquicheck.model.IdentifierType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NameConventionsCheckTest {

    private LiquiViolations violations;

    @Before
    public void init() {
        violations = new LiquiViolations();
    }

    @Test
    public void tableName_nomatch() {
        NameConventionsCheck check = new NameConventionsCheck();
        check.setTableName(new NamePattern("[A-Z]"));
        NameConvention event = check.create(violations);
        event.fire(new Identifier(IdentifierType.TABLE, "a"));
        event.end();
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void tableName_rename() {
        NameConventionsCheck check = new NameConventionsCheck();
        check.setTableName(new NamePattern("[A-Z]"));
        NameConvention event = check.create(violations);
        event.fire(new Identifier(IdentifierType.TABLE, "a"));
        event.fire(IdentifierType.TABLE, "a", "B");
        event.end();
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

    @Test
    public void tableName_match() {
        NameConventionsCheck check = new NameConventionsCheck();
        check.setTableName(new NamePattern("[A-Z]"));
        NameConvention event = check.create(violations);
        event.fire(new Identifier(IdentifierType.TABLE, "A"));
        event.end();
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

    @Test
    public void columnName_nomatch() {
        NameConventionsCheck check = new NameConventionsCheck();
        check.setColumnName(new NamePattern("[A-Z]"));
        NameConvention event = check.create(violations);
        event.fire(new Identifier(IdentifierType.COLUMN, "a"));
        event.end();
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void columnName_rename() {
        NameConventionsCheck check = new NameConventionsCheck();
        check.setColumnName(new NamePattern("[A-Z]"));
        NameConvention event = check.create(violations);
        event.fire(new Identifier(IdentifierType.COLUMN, "a"));
        event.fire(IdentifierType.COLUMN, "a", "A");
        event.end();
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

    @Test
    public void columnName_match() {
        NameConventionsCheck check = new NameConventionsCheck();
        check.setColumnName(new NamePattern("[A-Z]"));
        NameConvention event = check.create(violations);
        event.fire(new Identifier(IdentifierType.COLUMN, "A"));
        event.end();
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

}
