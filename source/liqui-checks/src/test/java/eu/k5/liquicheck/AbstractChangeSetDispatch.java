package eu.k5.liquicheck;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import eu.k5.liquicheck.events.Event;
import liquibase.exception.LiquibaseException;
import org.mockito.Mockito;

public class AbstractChangeSetDispatch {

    void check(Observers observer, String file) throws LiquibaseException {

        LiquiChecker checker = new LiquiChecker(new LiquiViolations(), observer);
        checker.check(file);

    }

    <T extends Event> T event(Observers observers, Class<T> type) {
        return observers.forType(type).iterator().next();
    }

    @SuppressWarnings("unchecked")
    Observers forEvents(Class<? extends Event>... events) {
        Builder<Class<?>, Event> builder = ImmutableMultimap.builder();

        for (Class<? extends Event> event : events) {
            builder.put(event, Mockito.mock(event));
        }
        return new Observers(builder.build());
    }
}
