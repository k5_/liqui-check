package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.checks.AllowedTypeCheck.AllowedType;
import eu.k5.liquicheck.checks.Limit;
import eu.k5.liquicheck.checks.AllowedTypeCheck.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AllowedTypesCheckTest {

    private LiquiViolations violations;
    private List<AllowedTypeCheck.Type> types;

    @Before
    public void init() {
        violations = new LiquiViolations();
        types = new ArrayList<>();
    }

    @Test
    public void unkownType() {
        addType("BIGINT");
        fire("CHAR");
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void unkownTypeMultiplicity() {
        addType("BIGINT");
        fire("CHAR", 2);
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void noMultiplicity() {
        addType("BIGINT");
        fire("BIGINT");
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

    @Test
    public void noMultiplicityButProvided() {
        addType("BIGINT");
        fire("BIGINT", 1);
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void missingMultiplicity() {
        addType("NVARCHAR", 1, 10);
        fire("NVARCHAR");
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void matchingMultiplicityLower() {
        addType("NVARCHAR", 1, 10);
        fire("NVARCHAR", 1);
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

    @Test
    public void matchingMultiplicityUpper() {
        addType("NVARCHAR", 1, 10);
        fire("NVARCHAR", 10);
        Assert.assertTrue(violations.getViolations().isEmpty());
    }

    @Test
    public void multiplicity_oneOver() {
        addType("NVARCHAR", 1, 10);
        fire("NVARCHAR", 11);
        Assert.assertEquals(1, violations.getViolations().size());
    }

    @Test
    public void multiplicity_oneUnder() {
        addType("NVARCHAR", 2, 10);
        fire("NVARCHAR", 1);
        Assert.assertEquals(1, violations.getViolations().size());
    }

    AllowedType check() {
        AllowedTypeCheck check = new AllowedTypeCheck();
        check.setTypes(types);
        return check.create(violations);
    }

    void fire(String name, Integer... mults) {
        check().fire(name, Arrays.asList(mults));
    }

    void addType(String name, int min, int max) {
        Type type = new Type();
        type.setName(name);
        Limit limit = new Limit();
        limit.setMin(min);
        limit.setMax(max);
        type.getLimits().add(limit);
        types.add(type);
    }

    private void addType(String name) {
        Type type = new Type();
        type.setName(name);
        types.add(type);
    }

}
