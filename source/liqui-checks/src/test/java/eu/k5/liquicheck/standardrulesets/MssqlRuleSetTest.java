package eu.k5.liquicheck.standardrulesets;

import eu.k5.liquicheck.CheckerBuilder;
import eu.k5.liquicheck.LiquiChecker;
import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.checks.StandardRuleSets;
import org.junit.Assert;
import org.junit.Test;

public class MssqlRuleSetTest {

    @Test
    public void basicChangesetIsValid() {
        LiquiChecker checker = new CheckerBuilder().addRuleSet(StandardRuleSets.MSSQL).build();
        LiquiViolations violations = checker.check("mssql/basic.xml");

        Assert.assertEquals(0, violations.getViolations().size());
    }
}
