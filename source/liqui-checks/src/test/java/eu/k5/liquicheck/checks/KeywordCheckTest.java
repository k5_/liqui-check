package eu.k5.liquicheck.checks;

import eu.k5.liquicheck.Environment;
import eu.k5.liquicheck.LiquiViolations;
import eu.k5.liquicheck.checks.KeywordCheck.Keyword;
import eu.k5.liquicheck.model.Identifier;
import eu.k5.liquicheck.model.IdentifierType;
import java.util.Arrays;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class KeywordCheckTest {

    private LiquiViolations violations;
    private Environment environment;

    @Before
    public void init() {
        violations = new LiquiViolations();
        environment = Mockito.mock(Environment.class);
    }

    @Test
    public void keyword() {
        KeywordCheck check = new KeywordCheck();
        check.setKeywords(Arrays.asList("END"));

        Keyword event = check.create(violations);
        event.start(environment);
        event.fire(new Identifier(IdentifierType.TABLE, "end"));

        Assert.assertEquals(1, violations.getViolations().size());
    }
}
