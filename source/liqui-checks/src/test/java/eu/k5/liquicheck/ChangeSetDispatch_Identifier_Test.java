package eu.k5.liquicheck;

import eu.k5.liquicheck.events.IdentifierEvent;
import eu.k5.liquicheck.events.RenameEvent;
import eu.k5.liquicheck.model.Identifier;
import eu.k5.liquicheck.model.IdentifierType;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

@SuppressWarnings("unchecked")
public class ChangeSetDispatch_Identifier_Test extends AbstractChangeSetDispatch {

    @Test
    public void identifier_column() throws LiquibaseException {
        Observers observers = forEvents(IdentifierEvent.class);

        check(observers, "dispatcher/identifiers/column.xml");

        IdentifierEvent event = event(observers, IdentifierEvent.class);

        verify(event, IdentifierType.COLUMN, "addColumn");
        verify(event, IdentifierType.COLUMN, "idColumn");
        verify(event, IdentifierType.COLUMN, "idColumnRename");
        verify(event, IdentifierType.TABLE, "idTable");

        Mockito.verifyNoMoreInteractions(event);
    }

    @Test
    public void identifier_table() throws LiquibaseException {
        Observers observers = forEvents(IdentifierEvent.class);

        check(observers, "dispatcher/identifiers/table.xml");
        IdentifierEvent event = event(observers, IdentifierEvent.class);

        verify(event, IdentifierType.TABLE, "idTable");
        verify(event, IdentifierType.TABLE, "newIdTable");
        verify(event, IdentifierType.COLUMN, "idColumn");

        Mockito.verifyNoMoreInteractions(event);
    }

    @Test
    public void identifier_rename_table() throws LiquibaseException {
        Observers observers = forEvents(RenameEvent.class);
        check(observers, "dispatcher/identifiers/table.xml");
        RenameEvent event = event(observers, RenameEvent.class);
        verify(event, IdentifierType.TABLE, "idTable", "newIdTable");
        Mockito.verifyNoMoreInteractions(event);
    }

    @Test
    public void identifier_view() throws LiquibaseException {
        Observers observers = forEvents(IdentifierEvent.class);

        check(observers, "dispatcher/identifiers/view.xml");
        IdentifierEvent event = event(observers, IdentifierEvent.class);

        verify(event, IdentifierType.VIEW, "newView");
        verify(event, IdentifierType.VIEW, "newViewRename");

        Mockito.verifyNoMoreInteractions(event);
    }

    @Test
    public void identifier_rename_view() throws LiquibaseException {
        Observers observers = forEvents(RenameEvent.class);

        check(observers, "dispatcher/identifiers/view.xml");
        RenameEvent event = event(observers, RenameEvent.class);
        verify(event, IdentifierType.VIEW, "newView", "newViewRename");
        Mockito.verifyNoMoreInteractions(event);
    }

    @Test
    public void identifier_constraint() throws LiquibaseException {
        Observers observers = forEvents(IdentifierEvent.class);

        check(observers, "dispatcher/identifiers/constraint.xml");
        IdentifierEvent event = event(observers, IdentifierEvent.class);

        verify(event, IdentifierType.UNIQUE_CONSTRAINT, "uniqueConstraint");
        verify(event, IdentifierType.PRIMERY_KEY_CONSTRAINT, "primaryKey");
        verify(event, IdentifierType.FOREIGN_KEY_CONSTRAINT, "fk_address_person");

        Mockito.verifyNoMoreInteractions(event);
    }

    private void verify(IdentifierEvent event, IdentifierType type, String id) {
        Mockito.verify(event).fire(new Identifier(type, id));
    }

    private void verify(RenameEvent event, IdentifierType type, String oldIdentifier, String newIdentifier) {
        Mockito.verify(event).fire(Matchers.eq(type), Matchers.eq(oldIdentifier), Matchers.eq(newIdentifier));
    }

}
