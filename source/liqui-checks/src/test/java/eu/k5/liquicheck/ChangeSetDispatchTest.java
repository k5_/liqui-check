package eu.k5.liquicheck;

import eu.k5.liquicheck.events.ColumnTypeEvent;
import java.util.Arrays;
import liquibase.exception.LiquibaseException;
import org.junit.Test;
import org.mockito.Mockito;

@SuppressWarnings("unchecked")
public class ChangeSetDispatchTest extends AbstractChangeSetDispatch {

    @Test
    public void columnType() throws LiquibaseException {

        Observers observers = forEvents(ColumnTypeEvent.class);

        check(observers, "dispatcher/basic.xml");

        ColumnTypeEvent event = event(observers, ColumnTypeEvent.class);
        Mockito.verify(event).fire("varchar", Arrays.asList(8));
        Mockito.verify(event).fire("decimal", Arrays.asList(2, 8));
        Mockito.verify(event).fire("bigint");
        Mockito.verifyNoMoreInteractions(event);

    }
}
