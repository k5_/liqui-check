package eu.k5.liquicheck;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import liquibase.exception.LiquibaseException;

public class BasicMain {

    public static void main(String[] args) throws JAXBException, LiquibaseException, IOException {

        String[] argv = {"-rules", "basic/rules.xml", "-changelog", "basic/changeset.xml"};

        Main.main(argv);
    }
}
