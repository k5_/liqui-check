package eu.k5.liquicheck;

import liquibase.exception.LiquibaseException;
import org.junit.Test;

public class TestCase {

    @Test
    public void test() throws LiquibaseException {

        LiquiChecker checker = new CheckerBuilder().fromClasspath("/basic/liquicheck.xml").build();
        checker.check("");

    }
}
