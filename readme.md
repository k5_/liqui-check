# Liquibase constraint checks

Some subtle errors in liquibase migrations are hard to find, 
especially when the changeset only fails on specific databases (version).
Changesets are hard to call back once in production.

This library tries to avoid some common mistakes made, that work on one database engine but not on others.

  * Checks for Keywords, in column/table/constraint names.
  * Checks identifier length, (e.g. Oracle just supports 30 characters)
  * Restriction to certain column types

Just configure what databases you intend to use, and let liquicheck verify you have no trivial problems.